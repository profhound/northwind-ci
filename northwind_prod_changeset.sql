ALTER TABLE public.suppliers ADD enabled BOOLEAN;

COMMENT ON COLUMN public.suppliers.enabled IS 'we should only use enabled suppliers';

ALTER TABLE public.orders ADD doc_id BIGINT;

ALTER TABLE public.nosql ALTER COLUMN  doc_id SET DEFAULT nextval('nosql_id_seq'::regclass);